APT_CONFIG=files/apt-config

TARGETS = $(APT_CONFIG)/etc/apt/apt.conf.d/01autoremove_badeehy

.PHONY: all
all: $(TARGETS)

.PHONY: clean
clean:
	$(RM) $(TARGETS)

# Keep metapackage dependencies as marked automatically installed
$(APT_CONFIG)/etc/apt/apt.conf.d/01autoremove_badeehy: \
 /etc/apt/apt.conf.d/01autoremove
	sed /metapackages/d $< > $@
